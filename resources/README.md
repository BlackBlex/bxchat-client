**BXChat Client**
===================


Es un chat escrito en java que contiene un sistema de plugins que complementerá el sistema.


*Requiere:* [BlexLib](https://bitbucket.org/BlackBlex/blexlib)

--------
**Caracteristicas:**

 - Enviar mensajes a todos los usuarios conectados.
 - Sistema de plugins.
 - Distinción de mensajes propios.

--------
**Plugins disponibles:**

 - **SendPMMessage** ~ Implementa un chat privado.
 - **SendFile** ~ Agrega la posibilidad de mandar archivos a un usuario en especifico.
 - **EmojiChooser** ~ Agrega un selector de emojis.
 - **FontChooser** ~ Agrega la posibilidad de cambiar el tamaño, el estilo de la fuente.

--------

**Nota:** *Se irá añadiendo más características conforme se vaya avanzando en su desarrollo. **No cuenta con sistema de seguridad, se implementará más adelante***



--------
 BXChat | Chat básico con sistema de plugins

 Cliente escrito en java

 Author: @BlackBlex (BlackBlex)

 License: General Public License (GPLv3) | http://www.gnu.org/licenses/