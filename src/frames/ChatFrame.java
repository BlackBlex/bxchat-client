/**
 * BXChat Client
 *
 * @author  Jovani P�rez Dami�n (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package frames
 *
 * ==============Information==============
 *      Filename: ChatFrame.java
 * ---------------------------------------
*/

package frames;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;

import com.blackblex.libs.application.components.borders.RoundedSidesBorder;
import com.blackblex.libs.application.components.styles.JButtonStyleFlat;
import com.blackblex.libs.net.objects.SocketMessage;
import com.blackblex.libs.net.objects.SocketUsername;
import com.blackblex.libs.net.objects.jMessage;
import com.blackblex.libs.system.utils.table.jMessageCell;
import com.blackblex.libs.system.utils.table.jMessageModel;
import com.blackblex.plugins.core.Core;
import com.blackblex.plugins.core.JPanelPluginContainer;

import app.ClientManager;
import app.Startup;

public class ChatFrame extends Frame
{

	private static final long serialVersionUID = 1239174552661529168L;

	public static String username = JOptionPane.showInputDialog("Nick: ");
	public static List <jMessage> jMessagesChat;
	public static jMessageModel jMessageModel;
	public static DefaultListModel <SocketUsername> jListUserModel;
	public static JTable jTableMessages;

	private JPopupMenu jMenuUserList;
	private JList <SocketUsername> jListUsers;
	private JScrollPane jscrollPaneTableMessages, jscrollPaneJListUsers, jscrollPaneJMessageChat;
	private JTextPane jMessageChat;
	private JPanelPluginContainer jPanelPluginContainer;
	private JButton jButtonSend;
	private JLabel jlabelChatUsername;

	public ChatFrame()
	{
		super();

		while ( username.isEmpty() )
		{
			try
			{
				username = JOptionPane.showInputDialog("Nick: ");
			}
			catch ( NullPointerException ex )
			{

			}
		}

		super.FRAME_WIDTH = (int) (super.screenSize.getWidth() * 0.45);
		super.FRAME_HEIGHT = (int) (super.screenSize.getHeight() * 0.65);

		finishInit();
	}

	@Override
	public void beforeInit()
	{
		super.beforeInit();

		jTableMessages = new JTable();
		jTableMessages.setName("jTableMessages");
		jTableMessages.setBackground(Color.WHITE);
		jTableMessages.setDefaultRenderer(jMessage.class, new jMessageCell());
		jTableMessages.setDefaultEditor(jMessage.class, new jMessageCell());
		jTableMessages.setRowHeight(100);
		jTableMessages.setShowGrid(false);
		jTableMessages.setRowMargin(4);
		jTableMessages.setTableHeader(null);

		this.jscrollPaneTableMessages = new JScrollPane();
		this.jscrollPaneTableMessages.setViewportView(jTableMessages);
		this.jscrollPaneTableMessages.setBackground(Color.WHITE);
		this.jscrollPaneTableMessages.setBorder(BorderFactory.createCompoundBorder(
				new RoundedSidesBorder(Color.darkGray, 3, 20, 20), BorderFactory.createEmptyBorder(10, 10, 10, 10)));

		jListUserModel = new DefaultListModel <>();

		this.jMenuUserList = new JPopupMenu()
		{
			private static final long serialVersionUID = 8033672417043884237L;

			@Override
			public void show(Component invoker, int x, int y)
			{
				int row = jListUsers.locationToIndex(new Point(x, y));
				if ( row != -1 )
				{
					jListUsers.setSelectedIndex(row);
				}
				super.show(invoker, x, y);
			}
		};
		this.jMenuUserList.setName("jMenuUserList");

		this.jListUsers = new JList <>();
		this.jListUsers.setModel(jListUserModel);
		this.jListUsers.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
		this.jListUsers.setComponentPopupMenu(jMenuUserList);
		this.jListUsers.setName("jListUsers");
		this.jListUsers.setMaximumSize(new Dimension((int) (super.FRAME_WIDTH * 0.17), super.jpanelBody.getHeight()));

		this.jscrollPaneJListUsers = new JScrollPane();
		this.jscrollPaneJListUsers.setViewportView(this.jListUsers);
		this.jscrollPaneJListUsers.setBackground(Color.WHITE);
		this.jscrollPaneJListUsers.setBorder(BorderFactory.createCompoundBorder(
				new RoundedSidesBorder(Color.darkGray, 1, 1, 1), BorderFactory.createEmptyBorder(10, 10, 10, 10)));
		this.jscrollPaneJListUsers
				.setPreferredSize(new Dimension((int) (super.FRAME_WIDTH * 0.2), super.jpanelBody.getHeight()));
		this.jscrollPaneJListUsers
				.setMinimumSize(new Dimension((int) (super.FRAME_WIDTH * 0.2), super.jpanelBody.getHeight()));

		jMessagesChat = new ArrayList <jMessage>();

		this.jMessageChat = new JTextPane();

		this.jscrollPaneJMessageChat = new JScrollPane(this.jMessageChat);
		this.jscrollPaneJMessageChat
				.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

		this.jPanelPluginContainer = new JPanelPluginContainer();
		this.jPanelPluginContainer.setBackground(Color.WHITE);
		this.jPanelPluginContainer.setName("jPanelPluginContainer1");

		this.jButtonSend = new JButton("Enviar");
		JButtonStyleFlat jButtonSendStyle = new JButtonStyleFlat(this.jButtonSend, "#2DCC70", 0);
		this.jButtonSend.setUI(jButtonSendStyle);

		this.jlabelChatUsername = new JLabel("<html>Chat de: <b>" + username + "</b></html>");
		this.jlabelChatUsername.setFont(new Font(this.jlabelChatUsername.getFont().getFontName(), Font.PLAIN, 16));
		this.jlabelChatUsername.setHorizontalAlignment(SwingConstants.CENTER);

		super.jpanelBody.setLayout(new BoxLayout(super.jpanelBody, BoxLayout.X_AXIS));

		super.jpanelBottom.setMaximumSize(new Dimension(super.FRAME_WIDTH, (int) (super.FRAME_HEIGHT * 0.15)));
		super.jpanelBottom.setPreferredSize(new Dimension(super.FRAME_WIDTH, (int) (super.FRAME_HEIGHT * 0.15)));
		super.jpanelBottom.setMinimumSize(new Dimension(super.FRAME_WIDTH, (int) (super.FRAME_HEIGHT * 0.15)));
	}

	@Override
	public void init()
	{
		super.init();

		super.jpanelHeader.add(this.jlabelChatUsername);

		super.jpanelBody.add(this.jscrollPaneTableMessages);
		super.jpanelBody.add(this.jscrollPaneJListUsers);

		super.gridbag.weightx = 1;
		super.gridbag.weighty = 1;
		super.gridbag.fill = GridBagConstraints.BOTH;
		super.jpanelBottom.add(this.jPanelPluginContainer, super.gridbag);

		super.gridbag.weightx = 0;
		super.gridbag.weighty = 0;
		this.jPanelPluginContainer.add(new JLabel("Mensaje: "), super.gridbag);

		super.gridbag.weightx = 1;
		super.gridbag.weighty = 1;
		super.gridbag.gridwidth = 1;
		super.gridbag.fill = GridBagConstraints.BOTH;
		super.gridbag.insets = new java.awt.Insets(0, 4, 0, 4);
		this.jPanelPluginContainer.add(this.jscrollPaneJMessageChat, super.gridbag);

		super.gridbag.weightx = 0;
		super.gridbag.weighty = 0;
		super.gridbag.insets = new java.awt.Insets(0, 0, 0, 4);
		this.jPanelPluginContainer.add(this.jButtonSend, super.gridbag);

		this.pack();
	}

	@Override
	public void afterInit()
	{
		this.addWindowListener(new java.awt.event.WindowAdapter()
		{
			public void windowClosing(java.awt.event.WindowEvent evt)
			{
				Startup.clientManager.disconnect();
				Startup.core.closePlugins();
				System.exit(1);
			}
		});

		this.jButtonSend.addActionListener(new java.awt.event.ActionListener()
		{
			public void actionPerformed(java.awt.event.ActionEvent evt)
			{
				putMessage();
			}
		});

	}

	@Override
	public void finishInit()
	{
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		super.jpanelHeader.setBackground(Color.WHITE);
		super.jpanelBody.setBackground(Color.WHITE);
		super.jpanelBottom.setBackground(Color.WHITE);

		Startup.clientManager = new ClientManager();
		Startup.clientManager.start();
		Core.globalService.setContainer(this.getContentPane());
		Core.globalService.addObject("SOCKETSERVER", Startup.clientManager.getSocketServer());
		Core.globalService.addObject("JMESSAGESCHAT", jMessagesChat);
		Core.globalService.addObject("JMESSAGE", jMessageChat);
		Core.globalService.addObject("JLISTUSERS", jListUsers);
		Core.globalService.addObject("JLISTUSERSMODEL", jListUserModel);
		Core.globalService.addObject("JMENUUSERLIST", jMenuUserList);

		setModel();

		super.showFrame("BXChat", "Cliente");
	}

	public void putMessage()
	{
		SocketMessage dataOutput = new SocketMessage();
		dataOutput.setFrom(Startup.clientManager.getSocketServer().getIdSession());
		dataOutput.setjMessage(jMessageChat.getDocument());
		dataOutput.setAction("send");
		dataOutput.setTo(-1);
		Startup.clientManager.getSocketServer().sendServer(dataOutput);

		jMessage jmes = new jMessage(0);
		jmes.setJMessage(dataOutput.getjMessage(), Startup.clientManager.getSocketServer());
		jMessagesChat.add(jmes);
		setModel();

		jMessageChat.setText("");
	}

	public static void setModel()
	{
		List <Object> items = new ArrayList <Object>();

		for ( Object i : jMessagesChat )
		{
			items.add(i);
		}

		jMessageModel = new jMessageModel(items);
		jTableMessages.setModel(jMessageModel);

		jTableMessages.changeSelection(items.size() - 1, 0, true, true);
	}

}
