/**
 * BXChat Client
 *
 * @author  Jovani P�rez Dami�n (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package app
 *
 * ==============Information==============
 *      Filename: ClientManager.java
 * ---------------------------------------
*/

package app;

import com.blackblex.libs.core.interfaces.custom.CommandInterface;
import com.blackblex.libs.net.objects.SocketMessage;
import com.blackblex.libs.net.objects.SocketUsername;
import com.blackblex.plugins.core.Core;

import frames.ChatFrame;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.net.UnknownHostException;

public class ClientManager extends Thread
{

    private String host = "127.0.0.1";
    private int puerto = 345;

    private SocketUsername socketServer = new SocketUsername();
    private ObjectInputStream objectInput;
    private SocketMessage dataInput = new SocketMessage(), dataOutput = new SocketMessage();

    public ClientManager()
    {
        Core.globalService.addObject("DATAINPUT", this.dataInput);

        try
        {
            this.socketServer.setSocketServer(new Socket(this.host, this.puerto));
            this.socketServer.setUsername(ChatFrame.username);
            this.dataOutput = new SocketMessage();
            this.dataOutput.setAction("nothing");
            this.socketServer.sendServer(this.dataOutput);
        }
        catch ( UnknownHostException e )
        {
            System.out.println("[ClientManager #1]: " + e.getMessage());
        }
        catch ( IOException e )
        {
            System.out.println("[ClientManager #1.1]: " + e.getMessage());
        }
    }

    public SocketUsername getSocketServer()
    {
        return this.socketServer;
    }

    public SocketMessage receive()
    {
        SocketMessage data = null;
        try
        {
            this.objectInput = new ObjectInputStream(this.socketServer.getSocketServer().getInputStream());
            data = (SocketMessage) this.objectInput.readObject();
        }
        catch ( IOException | ClassNotFoundException ex )
        {
        }

        return data;
    }

    public void disconnect()
    {
        if ( !this.socketServer.getSocketServer().isClosed() )
        {
            this.dataOutput = new SocketMessage();
            this.dataOutput.setAction("disconnect");
            this.dataOutput.setFrom(this.socketServer.getIdSession());
            this.dataOutput.setTo(-1);
            this.dataOutput.setMessage("bye");

            this.socketServer.sendServer(this.dataOutput);

            try
            {
                Thread.sleep(2000);
                this.socketServer.getSocketServer().close();
                this.interrupt();
            }
            catch ( IOException | InterruptedException e )
            {
                System.out.println("[ClientManager disconnect]: " + e.getMessage());
            }
        }
    }

    @Override
    public void run()
    {
        while ( !this.isInterrupted() )
        {
            if ( !this.socketServer.getSocketServer().isClosed() )
            {
                this.dataInput = receive();

                if ( this.dataInput != null )
                {
                    printData(this.dataInput);

                    if ( !this.dataInput.getAction().isEmpty() )
                    {
                        if ( Startup.commandList.containsKey(this.dataInput.getAction()) )
                        {
                            CommandInterface command = Startup.commandList.get(this.dataInput.getAction());
                            if ( command.canExecute(this.socketServer) )
                            {
                                command.execute(this.socketServer, this.dataInput);
                            }
                            ChatFrame.setModel();
                            this.dataInput.notifyObs();
                        }
                    }
                }
            }
        }
    }

    public void printData(SocketMessage data)
    {
        if ( data != null )
        {
            System.out.println("[" + data.getFrom() + " -> " + data.getTo() + "] " + data.getAction() + " - " + data.getMessage());
        }
    }

}
