/**
 * BXChat Client
 *
 * @author  Jovani P�rez Dami�n (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package app
 *
 * ==============Information==============
 *      Filename: Main.java
 * ---------------------------------------
*/

package app;

public class Main
{
    public static void main(String args[])
    {
        new Startup();
    }
}