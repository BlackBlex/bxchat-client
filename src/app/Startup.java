/**
 * BXChat Client
 *
 * @author  Jovani P�rez Dami�n (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package app
 *
 * ==============Information==============
 *      Filename: Startup.java
 * ---------------------------------------
*/

package app;

import com.blackblex.libs.core.interfaces.custom.CommandInterface;
import com.blackblex.plugins.core.Core;
import com.blackblex.plugins.core.GlobalService;
import commands.Add;
import commands.Disconnect;
import commands.Register;
import commands.SendMessage;
import commands.ServerFull;
import frames.ChatFrame;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.swing.JDialog;
import javax.swing.JFrame;

public class Startup
{

    public static ClientManager clientManager;
    public static Map<String, CommandInterface> commandList = new LinkedHashMap<>();
    public static Core core;

    public Startup()
    {
        try
        {
    		JFrame.setDefaultLookAndFeelDecorated(true);
    		JDialog.setDefaultLookAndFeelDecorated(true);
            javax.swing.UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        }
        catch ( ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex )
        {
            System.out.println("[Main]: " + ex.getMessage());
        }

        GlobalService global = new GlobalService();
        core = new Core("");
        core.setGlobalService(global);

        Core.globalService.addObject("COMMANDLIST", commandList);

        //#########Command list#########
	        commandList.put("send", new SendMessage());
	        commandList.put("register", new Register());
	        commandList.put("add", new Add());
	        commandList.put("full", new ServerFull());
	        commandList.put("disconnect", new Disconnect());
        //#########Command list#########

        new ChatFrame();

        core.exec();
    }
}
