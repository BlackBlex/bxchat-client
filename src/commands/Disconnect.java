/**
 * BXChat Client
 *
 * @author  Jovani P�rez Dami�n (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package commands
 *
 * ==============Information==============
 *      Filename: Disconnect.java
 * ---------------------------------------
*/

package commands;

import frames.ChatFrame;
import com.blackblex.libs.core.interfaces.custom.CommandInterface;
import com.blackblex.libs.net.objects.SocketMessage;
import com.blackblex.libs.net.objects.SocketUsername;
import com.blackblex.libs.net.objects.jMessage;

public class Disconnect implements CommandInterface
{

    @Override
    public String getDescription()
    {
        return "Notifica que usuario se ha desconectado";
    }

    @Override
    public boolean canExecute(SocketUsername socketUsername)
    {
        return true;
    }

    @Override
    public void execute(SocketUsername socketUsername, SocketMessage dataInput)
    {
        jMessage jmes = new jMessage(2);

        for ( int i = 0; i < ChatFrame.jListUserModel.getSize(); i++ )
        {
            SocketUsername user1 = (SocketUsername) ChatFrame.jListUserModel.getElementAt(i);
            if ( user1.getIdSession() == dataInput.getFrom() )
            {
                jmes.setMessage(user1.getUsername() + " se ha desconectado");
                ChatFrame.jListUserModel.remove(i);
                break;
            }
        }

        ChatFrame.jMessagesChat.add(jmes);
    }

}
