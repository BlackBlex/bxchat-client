/**
 * BXChat Client
 *
 * @author  Jovani P�rez Dami�n (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package commands
 *
 * ==============Information==============
 *      Filename: SendMessage.java
 * ---------------------------------------
*/

package commands;

import frames.ChatFrame;
import com.blackblex.libs.core.interfaces.custom.CommandInterface;
import com.blackblex.libs.net.objects.SocketMessage;
import com.blackblex.libs.net.objects.SocketUsername;
import com.blackblex.libs.net.objects.jMessage;

public class SendMessage implements CommandInterface
{

    @Override
    public String getDescription()
    {
        return "Recibe un mensaje de otros usuarios";
    }

    @Override
    public boolean canExecute(SocketUsername socketUsername)
    {
        return true;
    }

    @Override
    public void execute(SocketUsername socketUsername, SocketMessage dataInput)
    {
        SocketUsername user = null;
        for ( int i = 0; i < ChatFrame.jListUserModel.getSize(); i++ )
        {
            user = (SocketUsername) ChatFrame.jListUserModel.getElementAt(i);
            if ( user.getIdSession() == dataInput.getFrom() )
            {
                break;
            }
        }

        jMessage jmes = new jMessage(1);
        jmes.setJMessage(dataInput.getjMessage(), user);
        ChatFrame.jMessagesChat.add(jmes);
    }

}
