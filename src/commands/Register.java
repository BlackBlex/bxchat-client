/**
 * BXChat Client
 *
 * @author  Jovani P�rez Dami�n (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package commands
 *
 * ==============Information==============
 *      Filename: Register.java
 * ---------------------------------------
*/

package commands;

import frames.ChatFrame;
import com.blackblex.libs.core.interfaces.custom.CommandInterface;
import com.blackblex.libs.net.objects.SocketMessage;
import com.blackblex.libs.net.objects.SocketUsername;

public class Register implements CommandInterface
{

    @Override
    public String getDescription()
    {
        return "Se establece tu nombre al momento de conectarte al servidor";
    }

    @Override
    public boolean canExecute(SocketUsername socketUsername)
    {
        return true;
    }

    @Override
    public void execute(SocketUsername socketUsername, SocketMessage dataInput)
    {
        socketUsername.setIdSession(Integer.parseInt(dataInput.getMessage()));
        System.out.println("Mi id es: " + socketUsername.getIdSession());

        SocketMessage dataOutput = new SocketMessage();
        dataOutput.setAction("online");
        dataOutput.setFrom(socketUsername.getIdSession());
        dataOutput.setTo(-1);
        dataOutput.setMessage(ChatFrame.username);

        socketUsername.sendServer(dataOutput);
    }

}
