/**
 * BXChat Client
 *
 * @author  Jovani P�rez Dami�n (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package commands
 *
 * ==============Information==============
 *      Filename: Add.java
 * ---------------------------------------
*/

package commands;

import com.blackblex.libs.core.interfaces.custom.CommandInterface;
import com.blackblex.libs.net.objects.SocketMessage;
import com.blackblex.libs.net.objects.SocketUsername;
import com.blackblex.libs.net.objects.jMessage;

import frames.ChatFrame;

public class Add implements CommandInterface
{

    @Override
    public String getDescription()
    {
        return "Al conectarse un usuario se agrega a la lista de usuarios";
    }

    @Override
    public boolean canExecute(SocketUsername socketUsername)
    {
        return true;
    }

    @Override
    public void execute(SocketUsername socketUsername, SocketMessage dataInput)
    {
        SocketUsername user = new SocketUsername();
        user.setUsername(dataInput.getMessage());
        user.setIdSession(dataInput.getFrom());
        ChatFrame.jListUserModel.addElement(user);
        try
        {
            jMessage jmes = new jMessage(2);
            jmes.setMessage(user.getUsername() + " se ha unido al chat");
            ChatFrame.jMessagesChat.add(jmes);
        }
        catch ( Exception ex )
        {
        }
    }

}
