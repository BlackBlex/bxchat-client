/**
 * BXChat Client
 *
 * @author  Jovani P�rez Dami�n (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package commands
 *
 * ==============Information==============
 *      Filename: ServerFull.java
 * ---------------------------------------
*/

package commands;

import java.io.IOException;

import com.blackblex.libs.core.interfaces.custom.CommandInterface;
import com.blackblex.libs.net.objects.SocketMessage;
import com.blackblex.libs.net.objects.SocketUsername;
import com.blackblex.libs.net.objects.jMessage;

import app.Startup;
import frames.ChatFrame;

public class ServerFull implements CommandInterface
{

    @Override
    public String getDescription()
    {
        return "Si el servidor esta lleno se manda una notificaci�n al usuario.";
    }

    @Override
    public boolean canExecute(SocketUsername socketUsername)
    {
        return true;
    }

    @Override
    public void execute(SocketUsername socketUsername, SocketMessage dataInput)
    {
    	jMessage jmes = new jMessage(2);
        jmes.setMessage("Lo sentimos: el servidor esta lleno.");
        ChatFrame.jMessagesChat.add(jmes);
        ChatFrame.setModel();
        try
        {
            Thread.sleep(2000);
            socketUsername.getSocketServer().close();
            Startup.clientManager.interrupt();
        }
        catch ( IOException | InterruptedException e )
        {
            System.out.println("[ServerFull]: " + e.getMessage());
        }
    }

}
